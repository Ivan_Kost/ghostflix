import { AuthService } from '../services/auth.service';
import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private auth: AuthService,
                private router: Router) {}

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        // Get the auth token from the service.
        const authToken = this.auth.getAuthorizationToken();
        let authReq;
        let authorizedRequest = false;
        // Clone the request and replace the original headers with
        // cloned headers, updated with the authorization.


        if (authToken && authToken !== 'null') {
            authorizedRequest = true;
             authReq = req.clone({

                headers: req.headers.set('Authorization', 'Bearer ' + authToken)
                    .set('Accept', 'application/json')
            });
        } else {
            authReq = req.clone({
                headers: req.headers.set('Authorization', '')
                    .set('Accept', 'application/json')

            });
        }

        // send cloned request with header to the next handler.
        return next.handle(authReq).pipe( tap(() => {},
            (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status !== 401 && err.status !== 403) {
                        return;
                    }
                    if(authorizedRequest){
                        this.router.navigate(['restricted']);
                    }
                   return;
                }
            }));
    }
}
