import { Component, OnInit } from '@angular/core';
import { APP_CONFIG } from '../../app.config';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.page.html',
  styleUrls: ['./privacy.page.scss'],
})
export class PrivacyPage implements OnInit {
    logoImage: any;

    constructor() {
    }

    ngOnInit() {
        this.logoImage = APP_CONFIG.LOGO_IMAGE;
    }
}
