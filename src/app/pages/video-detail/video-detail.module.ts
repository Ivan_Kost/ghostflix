import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VideoDetailPageRoutingModule } from './video-detail-routing.module';

import { VideoDetailPage } from './video-detail.page';
import {StarRatingModule} from 'ionic5-star-rating';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        VideoDetailPageRoutingModule,
        StarRatingModule
    ],
  declarations: [VideoDetailPage]
})
export class VideoDetailPageModule {}
