import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { PlayerPage } from '../player/player.page';
import { VideoJSplayerPage } from '../video-jsplayer/video-jsplayer.page';
import {WordpressService} from '../../services/wordpress.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonfunctionService} from '../../services/commonfunction.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import {AuthService} from '../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.page.html',
  styleUrls: ['./video-detail.page.scss'],
})
export class VideoDetailPage implements OnInit {

  segId = 1;
  shows;
  loggedIn = false;
  video: any;
  videoSlug: string;
  comment: string;
  rating: any;
  existingScreenOrientation: string;
  loading: boolean = false;
  transName: any;
  awsUrl: any;

  constructor(
    private WP: WordpressService,
    private navCtrl: NavController,
    private modalController: ModalController,
    public translate: TranslateService,
    private CFS: CommonfunctionService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private so: ScreenOrientation) {
      this.transName = {
          add_watchlist: '',
          favorite: '',
          watchlist: '',
          add_favorite: '',
          episodes: '',
          season_1: ''
      };
  }

  ngOnInit() {
    this.isLoggedIn();
      this.translateData();
  }
  async translateData() {

      const trans = await this.translate.get([
          'SEASON.ADD_TO_WATCHLIST',
          'MORE.FAVORITE',
          'MORE.WATCHLIST',
          'SEASON.ADD_TO_FAVORITES',
          'SEASON.EPISODES',
          'SEASON.SEASON_1'
      ]).toPromise();
      if (trans) {
          this.transName = {
              add_watchlist: trans['SEASON.ADD_TO_WATCHLIST'],
              favorite: trans['MORE.FAVORITE'],
              watchlist: trans['MORE.WATCHLIST'],
              add_favorite: trans['SEASON.ADD_TO_FAVORITES'],
              episodes: trans['SEASON.EPISODES'],
              season_1: trans['SEASON.SEASON_1']
          };
      }
  }

  isLoggedIn() {
    this.auth.isUserLoggedIn().then((response) => {
      console.log(response);
      if(response['error']) {
          console.log('user not logged in');
          console.log('Error: ', response['error']);
          if (response['error'].type == 'true')
              this.router.navigateByUrl('/restricted');
      }if(response['code'] !=="jwt_auth_valid_token"){
        console.log('user not logged in')
        this.router.navigateByUrl('/restricted');
      }else{
          this.loggedIn = true;
          this.getVideoData();
      }
    });
  }

  ionViewWillEnter() {
    if(this.loggedIn){
      this.getVideoData();
    }else{
      this.isLoggedIn();
    }
  }

  getVideoData(){
    this.videoSlug = this.route.snapshot.paramMap.get('video');
    this.loading = true;
    this.WP.getVideo(this.videoSlug).subscribe((data) => {
      if (data === 'restricted') {
        console.log('data is restricted')
        this.router.navigate(['/restricted']);
      }
      if ( !data ){
        this.CFS.presentToast('error with this season, pls try later!', 3000);
        this.loading = false;
        this.navCtrl.back();
      }
      this.video = data;
      this.loading = false;
      //console.log(data);
    });
  }
  changeSegment(val) {
    this.segId = val;
  }

  goBack() {
    this.navCtrl.back();
  }

  async onPlay( item, awsTrailer ) {
      screen.orientation.unlock();
      screen.orientation.lock('landscape').then(() => {
          console.log('orientation is  supported');

      }).catch(err => console.log('Orientation error: ', err));
      //console.log('item:', item);
      if (awsTrailer) {
          this.WP.getSignUrl(item.aws_trailer, '', item.textrack_vtt).subscribe((data) => {
              //console.log('if ', data);
              this.goToModal(item, data, data.signed_url.texttrack, VideoJSplayerPage);
          });
      } else {
          console.log('ELSE!!! ', item);
          if (item.mp4_video) {
              //console.log('if ', item);
              this.WP.getSignUrl(item.mp4_video, item.mp4_video_hd, item.textrack_vtt).subscribe((data) => {
                  //console.log('data:', data);
                  this.goToModal(item, data, data.signed_url.texttrack, VideoJSplayerPage);
              });
          } else {
              //console.log('else ', item);
              this.goToModal(item, '', '', PlayerPage);
          }
      }
  }

    async goToModal(item, data, subtitleURL, playerType) {
        var mp4Video = (data != '' ? (data.signed_url.video ? data.signed_url.video : item.mp4_video) : item.mp4_video);
        var mp4Video_hd = (data != '' ? data.signed_url.video_hd : item.mp4_video_hd);
        var awsUrl_ = (data != '' ? data.signed_url.video : '');
        var videoID_ = (data != '' ? (!item.id ? item.video_id : item.id) : item.video_id);

        //console.log('* * * data: ', data);
        const modal = await this.modalController.create({
            component: playerType, //VideoJSplayerPage, //PlayerPage,
            cssClass: 'fullScreen',
            componentProps: {
                vimeoId: item.vimeo_video ? item.vimeo_video : item.video_embed_trailer,  // video_embed_trailer - a video in the Header
                mp4_video: mp4Video, //(data.signed_url.video ? data.signed_url.video : item.mp4_video), //item.mp4_video,
                mp4_video_hd: mp4Video_hd, // data.signed_url.video_hd,
                poster_image: item.poster_image,
                subtitlesOn: item.force_subtitles,
                videoId: videoID_,
                time: item.time ? item.time : 0,
                progress: item.progress,
                season: this.videoSlug,
                skipIntroTime: item.skip_intro_time,
                subtitle: subtitleURL,
                aws_url: awsUrl_ //data.signed_url.video //awsUrl
            }
        });
        modal.onDidDismiss().then((data) => {
            screen.orientation.unlock();
            //console.log('data modal video', data);
            if (data.data) {
                item.time = data.data.time;
                item.progress = data.data.progress;
                item.is_watched = data.data.is_watched;
            }
        });
        return await modal.present();

    }

  addToWatchList() {
    console.log('add to wish');
    const result = this.WP.addToWatchList(this.video.id).subscribe((data) => {
      if (data.success) {
        this.video.in_watchlist = data.in_watchlist;
      }
      //console.log(data);
    });
  }

  addToFavorites() {
    this.WP.addToFavorites(this.video.id).subscribe((data) => {
      if (data.success) {
        this.video.is_favorite = data.is_favorite;
      }
      //console.log(data);
    });
  }

  removeFromWatchList() {
    const result = this.WP.removeFromWatchList(this.video.id).subscribe((data) => {
      if (data.success) {
        this.video.in_watchlist = data.in_watchlist;
      }
      //console.log(data);
    });
  }

  removeFromFavorites() {
    this.WP.removeFromFavorites(this.video.id).subscribe((data) => {
      if (data.success) {
        this.video.is_favorite = data.is_favorite;
      }
      //console.log(data);
    });
  }

  logRatingChange(rating) {
    this.rating = rating;
    //console.log('rating', this.rating);
  }

  doComment() {
    const userId = localStorage.getItem('currentUserId');
    this.WP.pushComment(this.rating, this.comment, this.video.id, userId).subscribe((data) => {
      this.CFS.presentToast('Comment was sent, will appear soon!', 3000);
      this.rating = 0;
      this.comment = '';
    });
  }

  // Lock to portrait
  lockToPortrait() {
    this.so.lock(this.so.ORIENTATIONS.PORTRAIT);
  }

  // Lock to landscape
  lockToLandscape() {
    this.so.lock(this.so.ORIENTATIONS.LANDSCAPE);
  }
}
