import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import { APP_CONFIG } from '../../app.config';

@Component({
    selector: 'app-impressum',
    templateUrl: './impressum.page.html',
    styleUrls: ['./impressum.page.scss'],
})
export class ImpressumPage implements OnInit {
    logoImage: any;
    constructor(private router: Router) {
    }

    ngOnInit() {
        this.logoImage = APP_CONFIG.LOGO_IMAGE;
    }

    impressum() {
        this.router.navigate(['impressum']);
    }

    datenschutzerklarung() {
        this.router.navigate(['datenschutzerklarung']);
    }
}
