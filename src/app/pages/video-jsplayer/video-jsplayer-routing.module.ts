import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VideoJSplayerPage } from './video-jsplayer.page';

const routes: Routes = [
  {
    path: '',
    component: VideoJSplayerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VideoJSplayerPageRoutingModule {}
