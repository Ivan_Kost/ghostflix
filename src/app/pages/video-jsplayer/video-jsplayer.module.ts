import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VideoJSplayerPageRoutingModule } from './video-jsplayer-routing.module';

import { VideoJSplayerPage } from './video-jsplayer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VideoJSplayerPageRoutingModule
  ],
  declarations: [VideoJSplayerPage]
})
export class VideoJSplayerPageModule {}
