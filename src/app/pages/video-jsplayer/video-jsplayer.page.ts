import {Component, OnInit, OnDestroy, ViewChild, ElementRef, Input, ViewEncapsulation} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalController } from '@ionic/angular';

import { AndroidFullScreen } from '@ionic-native/android-full-screen/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

import { WordpressService } from '../../services/wordpress.service';
import { Platform } from '@ionic/angular';

import videojs from 'video.js';
import 'videojs-mobile-ui/dist/videojs-mobile-ui.css';
import 'videojs-mobile-ui';

import 'videojs-seek-buttons';
import 'videojs-contrib-quality-levels';

import 'videojs-hls-quality-selector/dist/videojs-hls-quality-selector.css';

export var global = {
    globalMP4 : '',
    globalMP4HD : '',
    SDHDname: '',
    globalTime: '',
    jsPlayer: ''
};

@Component({
  selector: 'app-video-jsplayer',
  templateUrl: './video-jsplayer.page.html',
  styleUrls: ['./video-jsplayer.page.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class VideoJSplayerPage implements OnInit, OnDestroy {
    @ViewChild('videojs', {static: false}) videoJS: ElementRef;
    @ViewChild('target', {static: true}) target: ElementRef;

    @Input() vimeoId: string;
    @Input() mp4_video: string;
    @Input() mp4_video_hd: string;

    @Input() poster_image: string;
    @Input() subtitlesOn: any;
    @Input() time: string;
    @Input() progress: string;
    @Input() season: string;
    @Input() videoId: any;
    @Input() videoSlug: any;
    @Input() skipIntroTime: any;
    @Input() aws_url: any;
    @Input() subtitle: any;

    destroyPage: boolean;
    controlActive = '';
    idPlayer: any;
    existingScreenOrientation: string;
    duration: any;
    p_progress: any;
    is_watched: any;
    platformName = '';
    hiddenSkipIntro: string;
    player: videojs;
    qualityLevels: any;

    mp4: any;
    SDHDname: any;

    constructor(private sanitizer: DomSanitizer,
                private WP: WordpressService,
                public modalCtrl: ModalController,
                private androidFullScreen: AndroidFullScreen,
                public platform: Platform,
                private elementRef: ElementRef, // for VideoJS
                private so: ScreenOrientation) {
        // get current orientation
        this.existingScreenOrientation = this.so.type;
    }

    ngOnInit() {
        // this is a very important parameter
        // will not to have 2 video players for ios !!!
        let playsinLine = (this.platformName == 'android') ? false : true;
        console.log('playsinLine: ', playsinLine);
        this.destroyPage = false;
        this.is_watched = false;
        this.hiddenSkipIntro = ''; //true;
        // this.skipIntroTime = false;
        console.log('this.skipIntroTime:', this.skipIntroTime);
        if (!this.skipIntroTime)
            this.skipIntroTime = 20;

        if (this.platform.is('desktop')) {
            console.log('desktop');
        } else if (this.platform.is('ipad')) {
            console.log('ipad');
            this.platformName = 'ipad';
        } else if (this.platform.is('iphone')) {
            console.log('iphone');
            this.platformName = 'iphone';
        } else if (this.platform.is('android')) {
            console.log('android');
            this.platformName = 'android';
        }

        if (this.aws_url) {
            this.mp4 = this.aws_url;
        }

        if (this.mp4_video) {
            this.SDHDname = 'SD';
        } else {
            this.SDHDname = 'HD';
        }

        global.globalMP4 = this.mp4_video;
        global.globalMP4HD = this.mp4_video_hd;
        global.SDHDname = this.SDHDname;
        if (this.time)
            global.globalTime = this.time;

        if (this.time >= this.skipIntroTime)
            this.hideElements(this.time);

        global.jsPlayer = this.player;
        this.videojsPlayer(playsinLine, this.player, global.globalTime);
    }

    videojsPlayer(playsinLine, jsPlayer, gTime) {

        var optionsJS = {
            //    fullscreen: true,
            autoplay: true,
            controls: true,
            currentTime: global.globalTime,
            muted: false,
            fluid: true,
            language: 'de',
            crossorigin: 'anonymous',
            playsinline: playsinLine,
            html5: {
                vhs: {experimentalBufferBasedABR: true, useDevicePixelRatio: true},
                nativeAudioTracks: false,
                nativeVideoTracks: false,
                useBandwidthFromLocalStorage: true
            },
            controlBar: {
                pictureInPictureToggle: false
            },
            sources: [{src: this.mp4, type: 'video/mp4', label: 'SD', res: '720'}],
            tracks: [{
                src: this.subtitle,
                kind: 'subtitles',
                srclang: 'de',
                label: 'Deutsch',
                default: (this.subtitlesOn == 'on') ? true : false
            }],
            hls: 1,
            languages: {
                de:
                    {
                        "Play": "Wiedergabe",
                        "Pause": "Pause",
                        "Replay": "Erneut abspielen",
                        "Current Time": "Aktueller Zeitpunkt",
                        "Duration": "Dauer",
                        "Remaining Time": "Verbleibende Zeit",
                        "Stream Type": "Streamtyp",
                        "LIVE": "LIVE",
                        "Loaded": "Geladen",
                        "Progress": "Status",
                        "Fullscreen": "Vollbild",
                        "Non-Fullscreen": "Vollbildmodus beenden",
                        "Mute": "Ton aus",
                        "Unmute": "Ton ein",
                        "Playback Rate": "Wiedergabegeschwindigkeit",
                        "Subtitles": "Untertitel",
                        "subtitles off": "Untertitel aus",
                        "Captions": "Untertitel",
                        "captions off": "Untertitel aus",
                        "Chapters": "Kapitel",
                        "You aborted the media playback": "Sie haben die Videowiedergabe abgebrochen.",
                        "A network error caused the media download to fail part-way.": "Der Videodownload ist aufgrund eines Netzwerkfehlers fehlgeschlagen.",
                        "The media could not be loaded, either because the server or network failed or because the format is not supported.": "Das Video konnte nicht geladen werden, da entweder ein Server- oder Netzwerkfehler auftrat oder das Format nicht unterstützt wird.",
                        "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "Die Videowiedergabe wurde entweder wegen eines Problems mit einem beschädigten Video oder wegen verwendeten Funktionen, die vom Browser nicht unterstützt werden, abgebrochen.",
                        "No compatible source was found for this media.": "Für dieses Video wurde keine kompatible Quelle gefunden.",
                        "Play Video": "Video abspielen",
                        "Close": "Schließen",
                        "Modal Window": "Modales Fenster",
                        "This is a modal window": "Dies ist ein modales Fenster",
                        "This modal can be closed by pressing the Escape key or activating the close button.": "Durch Drücken der Esc-Taste bzw. Betätigung der Schaltfläche \"Schließen\" wird dieses modale Fenster geschlossen.",
                        ", opens captions settings dialog": ", öffnet Einstellungen für Untertitel",
                        ", opens subtitles settings dialog": ", öffnet Einstellungen für Untertitel",
                        ", selected": ", ausgewählt",
                        "captions settings": "Untertiteleinstellungen",
                        "subtitles settings": "Einstellungen",
                        "descriptions settings": "Einstellungen für Beschreibungen",
                        "Close Modal Dialog": "Modales Fenster schließen",
                        "Descriptions": "Beschreibungen",
                        "descriptions off": "Beschreibungen aus",
                        "The media is encrypted and we do not have the keys to decrypt it.": "Die Entschlüsselungsschlüssel für den verschlüsselten Medieninhalt sind nicht verfügbar.",
                        ", opens descriptions settings dialog": ", öffnet Einstellungen für Beschreibungen",
                        "Audio Track": "Tonspur",
                        "Text": "Schrift",
                        "White": "Weiß",
                        "Black": "Schwarz",
                        "Red": "Rot",
                        "Green": "Grün",
                        "Blue": "Blau",
                        "Yellow": "Gelb",
                        "Magenta": "Magenta",
                        "Cyan": "Türkis",
                        "Background": "Hintergrund",
                        "Window": "Fenster",
                        "Transparent": "Durchsichtig",
                        "Semi-Transparent": "Halbdurchsichtig",
                        "Opaque": "Undurchsichtig",
                        "Font Size": "Schriftgröße",
                        "Text Edge Style": "Textkantenstil",
                        "None": "Kein",
                        "Raised": "Erhoben",
                        "Depressed": "Gedrückt",
                        "Uniform": "Uniform",
                        "Dropshadow": "Schlagschatten",
                        "Font Family": "Schriftfamilie",
                        "Proportional Sans-Serif": "Proportionale Sans-Serif",
                        "Monospace Sans-Serif": "Monospace Sans-Serif",
                        "Proportional Serif": "Proportionale Serif",
                        "Monospace Serif": "Monospace Serif",
                        "Casual": "Zwanglos",
                        "Script": "Schreibschrift",
                        "Small Caps": "Small-Caps",
                        "Reset": "Zurücksetzen",
                        "restore all settings to the default values": "Alle Einstellungen auf die Standardwerte zurücksetzen",
                        "Done": "Fertig",
                        "Caption Settings Dialog": "Einstellungsdialog für Untertitel",
                        "Beginning of dialog window. Escape will cancel and close the window.": "Anfang des Dialogfensters. Esc bricht ab und schließt das Fenster.",
                        "End of dialog window.": "Ende des Dialogfensters.",
                        "Audio Player": "Audio-Player",
                        "Video Player": "Video-Player",
                        "Progress Bar": "Fortschrittsbalken",
                        "progress bar timing: currentTime={1} duration={2}": "{1} von {2}",
                        "Volume Level": "Lautstärke",
                        "{1} is loading.": "{1} wird geladen.",
                        "Seek to live, currently behind live": "Zur Live-Übertragung wechseln. Aktuell wird es nicht live abgespielt.",
                        "Seek to live, currently playing live": "Zur Live-Übertragung wechseln. Es wird aktuell live abgespielt.",
                        "Exit Picture-in-Picture": "Bild-im-Bild-Modus beenden",
                        "Picture-in-Picture": "Bild-im-Bild-Modus"

                    }
            }
        };
        
        if (global.globalMP4HD) {
            var Button = videojs.getComponent('Button');
            var MyButton = videojs.extend(Button, {
                constructor: function () {
                    Button.apply(this, arguments);
                    this.addClass('vjs-icon-hd');

                },
                handleClick: function (event) {
                    // do something on click

                    let newTime = this.player_.currentTime();
                    if (newTime > global.globalTime) {
                        global.globalTime = newTime;
                    }

                    if (global.SDHDname == 'SD') {
                        global.SDHDname = 'HD';

                        this.addClass('vjs-icon-hd-blue');
                        this.player_.src({
                            src: global.globalMP4HD,
                            type: 'video/mp4',
                            label: 'HD',
                            res: '1080'
                        });
                        this.player_.currentTime(global.globalTime);
                        this.player_.play();

                    } else {
                        global.SDHDname = 'SD';
                        this.removeClass('vjs-icon-hd-blue');
                        this.player_.src({
                            src: global.globalMP4,
                            type: 'video/mp4',
                            label: 'SD',
                            res: '720'
                        });
                        this.player_.currentTime(global.globalTime);
                        this.player_.play();
                    }
                }
            });
            videojs.registerComponent('MyButton', MyButton);
        }
        jsPlayer = videojs('video-jsplayer-id', optionsJS, function onPlayerReady() {
            videojs.log('Your player is ready!');
            this.play();
            this.on('ended', function () {
                videojs.log('Awww...over so soon?!');
            });
        });
        jsPlayer.language('de');
        jsPlayer.getChild('controlBar').addChild('myButton', {}, 9);
        jsPlayer.play();
       // jsPlayer.requestFullscreen();
        jsPlayer.on('loaded', () => {
            console.log('global.globalTime: ', global.globalTime);
            jsPlayer.currentTime(global.globalTime);
        });

        jsPlayer.seekButtons({
            forward: 10, // 10 sec ->
            back: 10     // 10 sec <-
        });
        /*
            jsPlayer.on('resolutionchange', function () {
                console.log('Source changed to %s', jsPlayer.src());
                // this.player.currentTime(global.globalTime);
            })
        */
        global.jsPlayer = jsPlayer;
    }

    ionViewWillEnter() {
        let time = this.getTime();
        // console.log('time: ', time);

        if (global.jsPlayer) {
            this.player = global.jsPlayer;
        }

        if (time) {
            //this.hideElements(time);
            this.player.currentTime(time);
        } else {
            this.hiddenSkipIntro = '';
        }

        this.player.on('loaded', () => {
            console.log('global.globalTime: ', global.globalTime);
            if (global.globalTime >= this.player.currentTime())
                this.player.currentTime(global.globalTime);
        });
    }

    skipIntro() {
        console.log('this.player=', this.player);
        this.time = this.player.currentTime();
        console.log('skipIntro time : ', this.time);

        if (!this.hiddenSkipIntro) {
            this.time += 20;
            console.log('skipIntroTime+20: ', this.time);
            this.player.currentTime(this.time);
            this.hiddenSkipIntro = 'hidden';
        }
        //this.hiddenSkipIntro = 'hidden';
    }

    hideElements(seconds) {
        console.log('hide elements', this.skipIntroTime, seconds, seconds >= this.skipIntroTime);
        if (seconds >= this.skipIntroTime) {
            this.hiddenSkipIntro = 'hidden';//false;
        } else {
            this.hiddenSkipIntro = '';//true;
        }
        console.log(' this.hiddenSkipIntro:', this.hiddenSkipIntro);
    }

    async videoEnd() {
        this.duration = this.player.duration();
        let time_ = this.player.currentTime();
        global.globalTime = time_;

        const result = this.saveTime(time_);
        if (result) {
            this.ngOnDestroy();
        }
    }

    playingVideo() {
        console.log('play video');
    }

    showControls() {
        console.log('show controls');
        this.controlActive = 'active';
        setTimeout(() => {
            this.controlActive = '';
        }, 3000);

    }


    async saveTime(time) {
        this.destroyPage = true;
        if (!this.duration)
            this.duration = this.player.duration();

        //console.log('Save this.season ', this.season);
        //console.log('Save videoId ', this.videoId);
        //console.log('Save duration ', this.duration);

        this.WP.saveTime(this.season, this.videoId, time, this.duration)
            .subscribe((data) => {
                console.log('saved responce ', data);
                if (data.success) {
                    this.p_progress = (data.progress == 99) ? 100 : data.progress;
                    this.time = data.time;
                }

                this.modalCtrl.dismiss({is_watched: this.is_watched, time: this.time, progress: this.p_progress});
                return true;
            }, error => {
                // there are no more posts available
                console.log('error: ', error);
                this.modalCtrl.dismiss({is_watched: this.is_watched, time: this.time, progress: this.p_progress});
                // an error occurred
                return false;
            });
    }

    getTime() {
        let old_time = localStorage.getItem('vimeo_time_' + this.vimeoId);
        let time = this.time;
        if (old_time && old_time !== '') {
            time = old_time;
            localStorage.setItem('vimeo_time_' + this.vimeoId, '');
        }
        return time;
    }

    setVideoWatched() {
        console.log('set video as watched');
        this.WP.setVideoAsWatched(this.season, this.videoId).subscribe((data) => {
            console.log(data);
            if (data.success) {
                for (let i = 0; i < data.watched_videos.length; i++) {
                    if (data.watched_videos[i] == this.videoId) {
                        this.is_watched = true;
                        this.p_progress = 100;
                        return;
                    }
                }
            }
        });
    }

    ngOnDestroy() {
        // let time_ = this.player.currentTime();
        // time_ = (time_ > global.globalTime) ? time_ : global.globalTime;
        //  this.saveTime(global.globalTime);
        console.log('this.player: ', this.player);
        // destroy player
        if (this.player.isDisposed_ != true) {
            this.player.dispose();
        }
    }
}
