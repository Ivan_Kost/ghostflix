import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {CommonfunctionService} from '../../services/commonfunction.service';
import {AuthService} from '../../services/auth.service';
import {Platform} from '@ionic/angular';
import { APP_CONFIG } from '../../app.config';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string = '';
  password: string = '';
  currentUserId: any;
  logoImage: any;

  constructor(
      private CFS: CommonfunctionService,
      private auth: AuthService,
      private router: Router,
      private platform: Platform) {

  }

  ngOnInit() {
      const accessToken = localStorage.getItem('access_token');
      //  this.isLoggedIn();
      this.logoImage = APP_CONFIG.LOGO_IMAGE;
  }

  register() {
    this.router.navigate(['register']);
  }
  goToSelectUser() {
    this.router.navigate(['/select-user']);
  }

  isLoggedIn() {
    let currentSessionUser = localStorage.getItem('access_token');
    if(!currentSessionUser || currentSessionUser === 'null'){
      return false;
    }
    this.auth.isUserLoggedIn().then((response) => {
      console.log(response)
      if (response['error']) {
        console.log('user not logged in');
      } else {
        if(response['code'] ==="jwt_auth_valid_token") {
            // this.router.navigateByUrl('/tabs/staffeln');
            this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
                this.router.navigate(['/tabs/staffeln']));
        }
      }
    });
  }

  login() {
    if (this.CFS.validateEmail(this.email)  && (this.password !== ''))   {
      this.auth.login(this.email, this.password).then((response) => {
        if (response['error']) {
          this.CFS.presentToast(response['error'].message, 3000);
        } else {
          this.CFS.presentToast('Du hast dich erfolgreich angemeldet!',1000);
          localStorage.setItem('access_token', response['token']);
          localStorage.setItem('display_name', response['user_display_name']);
          this.CFS.setDisplayName(response['user_display_name']);

          // CUSTOM HACK decode current user ID from token
          let jwt = response['token'];
          let jwtData = jwt.split('.')[1];
          let decodedJwtJsonData = window.atob(jwtData);
          let decodedJwtData = JSON.parse(decodedJwtJsonData);
          this.currentUserId = decodedJwtData.data.user.id;
          localStorage.setItem('currentUserId', this.currentUserId);
          console.log('redirect to staffeln!!!');
          //this.router.navigateByUrl('/tabs/staffeln');
            localStorage.setItem('reload', 'true');
            this.router.navigateByUrl('/tabs/staffeln', { replaceUrl: true });
            window.location.replace('/tabs/staffeln');
           // window.location.reload();
            /*
          this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
              this.router.navigate(['/tabs/staffeln'], { name: "login" }));
           // window.location.reload();
           */
        }
      });
    } else {
      this.CFS.presentToast('Either email or password is wrong!', 3000);
    }
  }

    privacy(){
        this.router.navigate(['/privacy']);
    }
    impressum() {
        this.router.navigate(['/impressum']);
    }

    datenschutzerklarung() {
        this.router.navigate(['/datenschutzerklarung']);
    }
}

