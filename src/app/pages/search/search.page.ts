import { Component, OnInit } from '@angular/core';
import { WordpressService } from '../../services/wordpress.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
    videos: any;
    videosGlobal: any;
    isItemAvailable: false;
    topVal: boolean;
    title: any;
    continueWatching: any

    constructor(
        private WP: WordpressService,
        private navCtrl: NavController,
        public translate: TranslateService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.topVal = false;
        this.getTop10Sliders();

        this.translateData();
    }
    async translateData() {

        const trans = await this.translate.get([
            'SEARCH.TITLE',
            'SEARCH.POPULAR_SEARCHES'
        ]).toPromise();
        if (trans) {
            this.title = trans['SEARCH.TITLE'];
            this.continueWatching = trans['SEARCH.POPULAR_SEARCHES'];
        }
    }

    getSearchData(keyword: any) {
        const val = keyword.target.value;
        console.log(val);
        this.topVal = true;
        this.WP.getSearchData(val).subscribe((data) => {
            console.log(data);
            this.videos = data.items;
        });
    }

    ionViewWillEnter() {
    }

    goToVideoDetail(slug) {
        console.log('slug:', slug);
        this.router.navigate(['tabs/video-detail', slug]).then();
    }

    getTop10Sliders() {
        this.WP.getTypeSlide('top-ten').subscribe((data) => {
            this.videos = data;
        });
    }

    goBack() {
        this.navCtrl.back();
    }
}