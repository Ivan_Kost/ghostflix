import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import { APP_CONFIG } from '../../app.config';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.page.html',
  styleUrls: ['./video-list.page.scss'],
})
export class VideoListPage implements OnInit {

  index;
  type: string;
  logoImage: any;
  reloadPage: any;
  constructor(private router: Router) {}

  ngOnInit() {
    this.reloadPage = false;
    this.logoImage = APP_CONFIG.LOGO_IMAGE;
    const splurl = this.router.url.split('/');
    this.type = splurl[splurl.length - 1];
    console.log('on video-list init: type', this.type);
  }
  searchPage(){
      this.router.navigate(['tabs/search']).then();
  }
  ionViewWillEnter() {
      var reload_ = localStorage.getItem('reload');
      if (reload_ == 'false') {
          window.location.reload();
          localStorage.setItem('reload', 'true');
      }
  }
}
