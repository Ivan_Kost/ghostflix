import { Component, OnInit } from '@angular/core';
import {WordpressService} from '../../services/wordpress.service';
import {Router} from '@angular/router';
import {AuthService} from "../../services/auth.service";
import { APP_CONFIG } from '../../app.config';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit {

  userId: any;
  userData: any;
  segId = 1;
  userVideoData: any;
  logoImage: any;
  transName: any;

  constructor(
      public translate: TranslateService,
      private WP: WordpressService,
      private router: Router,
      private auth: AuthService) {
      console.log('init user info page');
      this.transName = {
          add_watchlist: '',
          favorite: '',
          watchlist: '',
          add_favorite: '',
          episodes: '',
          logout: ''
      };
  }

  ngOnInit() {
    this.isLoggedIn();
    this.logoImage = APP_CONFIG.LOGO_IMAGE;
    this.translateData();
  }
  async translateData() {

      const trans = await this.translate.get([
          'SEASON.ADD_TO_WATCHLIST',
          'MORE.FAVORITE',
          'MORE.WATCHLIST',
          'SEASON.ADD_TO_FAVORITES',
          'SEASON.EPISODES',
          'MORE.LOGOUT'
      ]).toPromise();
      if (trans) {
          this.transName = {
              add_watchlist: trans['SEASON.ADD_TO_WATCHLIST'],
              favorite: trans['MORE.FAVORITE'],
              watchlist: trans['MORE.WATCHLIST'],
              add_favorite: trans['SEASON.ADD_TO_FAVORITES'],
              episodes: trans['SEASON.EPISODES'],
              logout: trans['MORE.LOGOUT']
          };
      }
  }

  isLoggedIn() {
    this.auth.isUserLoggedIn().then((response) => {
      console.log(response);
      if(response['code'] !=="jwt_auth_valid_token"){
        console.log('user not logged in');
        this.router.navigateByUrl('/restricted');
      }
    });
  }

  updateData() {
    console.log('update user data');
    this.WP.getUserData(this.userId).subscribe((data) => {
      this.userData = data;
    });
    this.WP.getUserVideoData().subscribe((data) => {
      this.userVideoData = data;
    });
  }

  ionViewWillEnter() {

    console.log('on ion view enter event fired');
    this.userId = localStorage.getItem('currentUserId');
    console.log('User id', this.userId);
    this.updateData();
  }

  changeSegment(val) {
    this.segId = val;
  }

  details(slug) {
    this.router.navigate(['tabs/video-detail', slug]).then();
  }

  logout() {
    localStorage.setItem('currentUserId', null);
    localStorage.setItem('access_token', null);
    localStorage.setItem('display_name', null);
    localStorage.setItem('reload', null);
    this.router.navigateByUrl('/login');
  }
}
