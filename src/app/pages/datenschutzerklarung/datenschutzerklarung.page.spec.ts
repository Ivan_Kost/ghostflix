import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DatenschutzerklarungPage } from './datenschutzerklarung.page';

describe('DatenschutzerklarungPage', () => {
  let component: DatenschutzerklarungPage;
  let fixture: ComponentFixture<DatenschutzerklarungPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DatenschutzerklarungPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DatenschutzerklarungPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
