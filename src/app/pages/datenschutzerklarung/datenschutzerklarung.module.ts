import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DatenschutzerklarungPageRoutingModule } from './datenschutzerklarung-routing.module';

import { DatenschutzerklarungPage } from './datenschutzerklarung.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DatenschutzerklarungPageRoutingModule
  ],
  declarations: [DatenschutzerklarungPage]
})
export class DatenschutzerklarungPageModule {}
