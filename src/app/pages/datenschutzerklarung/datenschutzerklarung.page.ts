import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { APP_CONFIG } from '../../app.config';

@Component({
    selector: 'app-datenschutzerklarung',
    templateUrl: './datenschutzerklarung.page.html',
    styleUrls: ['./datenschutzerklarung.page.scss'],
})
export class DatenschutzerklarungPage implements OnInit {
    logoImage: any;
    constructor(private router: Router) {
    }

    ngOnInit() {
        this.logoImage = APP_CONFIG.LOGO_IMAGE;
    }

    impressum() {
        this.router.navigate(['impressum']);
    }

    datenschutzerklarung() {
        this.router.navigate(['datenschutzerklarung']);
    }
}
