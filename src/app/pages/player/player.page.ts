import {Component, OnInit, ViewChild, ElementRef, Input} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import { ModalController } from '@ionic/angular';
import Player from '@vimeo/player';
import { AndroidFullScreen } from '@ionic-native/android-full-screen/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { WordpressService } from '../../services/wordpress.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-player',
  templateUrl: './player.page.html',
  styleUrls: ['./player.page.scss'],
})
export class PlayerPage implements OnInit {
  @ViewChild('videoPlayer', { static: false }) mVideoPlayer: ElementRef;
  @ViewChild('iframe', { static: false }) mIframe: ElementRef;
  @ViewChild('vimeoEl', { static: false }) vimeoEl: ElementRef;
  @Input() vimeoId: string;
  @Input() subtitlesOn: any;
  @Input() time: string;
  @Input() progress: string;
  @Input() season: string;
  @Input() videoId: any;
  @Input() videoSlug: any;
  @Input() skipIntroTime: any;
  @Input() aws_url: any;

  controlActive = '';
  idPlayer: any;
  existingScreenOrientation: string;
  duration: any;
  p_progress: any;
  is_watched: any;
  platformName = '';
  hiddenSkipIntro: boolean;
  is_tablet = false;

  constructor(private sanitizer: DomSanitizer,
              private WP: WordpressService,
              public modalCtrl: ModalController,
              private androidFullScreen: AndroidFullScreen,
              public platform: Platform,
              private so: ScreenOrientation) {
    // get current orientation
    this.existingScreenOrientation = this.so.type;

  }

  ngOnInit() {
    this.is_watched = false;
    this.hiddenSkipIntro = false;
      if (!this.skipIntroTime)
          this.skipIntroTime = 20;

      if (this.platform.is('desktop')) {
          console.log('desktop');
      }else if (this.platform.is('ipad')) {
          console.log('ipad');
          this.platformName = 'ipad';
      }else if (this.platform.is('iphone')) {
          console.log('iphone');
          this.platformName = 'iphone';
      }else if (this.platform.is('android')) {
          console.log('android');
          this.platformName = 'android';
      }
      this.is_tablet = this.platform.is('tablet');


  }

  ionViewWillEnter() {
    // this is a very important parameter
    // will not to have 2 video players for ios !!!
    // let playsinLine =(this.platformName=='android')?false:true;
    let playsinLine = (this.platformName == 'android' || this.platformName == 'ipad') ? false : true;
    this.idPlayer = new Player('vimeo-el', {
      id: this.vimeoId,
      height: window.innerHeight,
      autoplay: true,
      playsinline: playsinLine,
      fullscreen: true,
      texttrack: 'de',
        transparent: 0,
        style: 'background-color: black;'
    });

    if(this.subtitlesOn === 'on') {
      console.log('this.subtitlesOn on: ', this.subtitlesOn);

      this.idPlayer.enableTextTrack('de').then(function (track) {
        console.log('$$$$$   track: ', track);
          track.language = 'de';
          track.kind='captions';
          track.label='Deutsch CC';
        // the track was disabled
      }).catch(function (error) {
        // an error occurred
          console.log('enableTextTrack ERROR: ', error);
      });
    }else{
  /*      console.log('this.subtitlesOn not on: ', this.subtitlesOn);
  */    this.idPlayer.disableTextTrack().then(function() {
        // the track was disabled
      }).catch(function(error) {
        // an error occurred
      });
    }

    this.idPlayer.on('play', () => {
      this.idPlayer.getCurrentTime().then(seconds => {
        this.hideElements(seconds);

      }).catch(error => {
        // an error occurred
      });
    });
    this.idPlayer.on('timeupdate', (data) => {
      this.hideElements(data.seconds);
    });
    this.idPlayer.on('ended', () => {
      this.is_watched = true;
      this.setVideoWatched();
    })
    this.idPlayer.on('loaded', () => {
    /*  screen.orientation.lock('landscape').
      then(() => console.log('orientation is  supported'))
          .catch(err => console.log('Orientation error: ', err));
      console.log('Locked Orientation is ' + screen.orientation.type);*/
      let time = this.getTime();
      if ( time ) {
     /*     console.log('TIME: ', time);
          console.log('skipIntroTime:', this.skipIntroTime);
          console.log('Number(time):', Number(time));
     */     this.hideElements(time);

          if(this.subtitlesOn === 'on') {
              this.idPlayer.enableTextTrack('de', 'subtitles');
          }else{
              this.idPlayer.disableTextTrack();
          }
          this.idPlayer.setCurrentTime(time).then(function () {
          }).catch(function (error) {
              // an error occurred
          });
          if (Number(time) <= 20) {
              this.hiddenSkipIntro = true;
          }
          //this.hideElements(Number(time));
      }else{
        this.hiddenSkipIntro = true;
      }
      //if(this.platformName=='iphone')
      //  this.vimeoEl.nativeElement.getElementsByTagName('iframe')[0].style.height = '80%';
      //else
        this.vimeoEl.nativeElement.getElementsByTagName('iframe')[0].style.width = '100%';
        this.vimeoEl.nativeElement.getElementsByTagName('iframe')[0].style.height = '100%';
        this.vimeoEl.nativeElement.getElementsByTagName('iframe')[0].style.maxHeight = '100vh';
        this.vimeoEl.nativeElement.getElementsByTagName('iframe')[0].style.backgroundColor = '#000000';
        if(this.platform.is('android')
        && this.platform.is('mobile')
        && this.vimeoEl.nativeElement.getElementsByTagName('iframe')[0].parentElement.style.paddingTop == '56.25%'){
          this.vimeoEl.nativeElement.getElementsByTagName('iframe')[0].parentElement.style.paddingTop = '100vh';
        }

        if(this.platform.is('ipad')
            && this.vimeoEl.nativeElement.getElementsByTagName('div')[1]) {
            this.vimeoEl.nativeElement.getElementsByTagName('div')[1].style.height = '100%';
        }
    });

    this.idPlayer.getDuration().then(seconds => {
      this.duration = seconds;
    }).catch(error => {
      // an error occurred
    });
    //setTimeout(() => {
    //  this.goFullScreen();
    //}, 1000);
    /*  this.androidFullScreen.leanMode().then(() => console.log('Lean mode supported'))
        .catch(err => console.log(err));*/
    console.log('player initialized');
  }
    /*
  goFullScreen() {
    this.idPlayer.requestFullscreen().then(function() {
      console.log('Player in full screen mode');
    }).catch(function(error) {
      console.log('fullscreen error : ',error);
    });
  }
*/
   async getCurrentTime(currentTime_) {
       return this.idPlayer.getCurrentTime().then(function (currentTime_) {
           return currentTime_;
       });
   }

    async skipIntro() {
        let currentTime = await this.getCurrentTime(0);
        if (currentTime) {
            this.idPlayer.setCurrentTime(this.skipIntroTime).then(function (seconds) {
                console.log('seconds :', seconds);
            }).catch(function (error) {
                console.log('error :', error);
            });
            this.hiddenSkipIntro = false;
        }
    }

    hideElements(seconds) {
        //console.log('hide elements', this.skipIntroTime, seconds, seconds >= this.skipIntroTime)
        if (seconds >= this.skipIntroTime) {
            this.hiddenSkipIntro = false;
        } else {
            this.hiddenSkipIntro = true;
        }
    }

    videoEnd() {
      this.idPlayer.getCurrentTime().then(seconds => {
        this.saveTime(seconds);

      }).catch(error => {
        // an error occurred
      });
    }

  playingVideo() {
    console.log('play video');
  }

  showControls() {
    this.controlActive = 'active';
    setTimeout(() => {
      this.controlActive = '';
    }, 3000);

  }

  getVimeoUrl() {
    return this.sanitizer.bypassSecurityTrustResourceUrl('https://player.vimeo.com/video/' + this.vimeoId);
  }

  saveTime(time) {
    console.log('Save time');
    this.WP.saveTime(this.season, this.videoId,time,this.duration).subscribe( (data) => {
      if(data.success){
        this.p_progress = (data.progress == 99) ? 100 : data.progress;
        this.time = data.time;
      }
      this.modalCtrl.dismiss({is_watched:this.is_watched, time:this.time,progress: this.p_progress});
    }).catch((error) => {
      console.log('error: ',error);
      this.modalCtrl.dismiss({is_watched:this.is_watched, time:this.time,progress: this.p_progress});
      // an error occurred
    });
  }

  getTime() {
    let old_time = localStorage.getItem('vimeo_time_' + this.vimeoId);
    let time = this.time;
    if(old_time && old_time !== ''){
      time = old_time;
      localStorage.setItem('vimeo_time_' + this.vimeoId, '');
    }
    return  time;
  }

  setVideoWatched() {
      console.log('set video as watched');
      this.WP.setVideoAsWatched(this.season, this.videoId).subscribe((data) => {
          console.log(data);
          if (data.success) {
              for (let i = 0; i < data.watched_videos.length; i++) {
                  if (data.watched_videos[i] == this.videoId) {
                      this.is_watched = true;
                      this.p_progress = 100;
                      return;
                  }
              }
          }
      });
  }

}
