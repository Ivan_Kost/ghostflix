import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';

import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

import { AppComponent } from './app.component';

const TRANSLATIONS_DE = require('../assets/i18n/de.json');
const TRANSLATIONS_EN = require('../assets/i18n/en.json');

describe('AppComponent', () => {

  let statusBarSpy;
  let splashScreenSpy;
  let platformReadySpy;
  let platformSpy;

  let translate: TranslateService;
  let http: HttpTestingController;

  beforeEach(waitForAsync(() => {
    statusBarSpy = jasmine.createSpyObj('StatusBar', ['styleDefault']);
    splashScreenSpy = jasmine.createSpyObj('SplashScreen', ['hide']);
    platformReadySpy = Promise.resolve();
    platformSpy = jasmine.createSpyObj('Platform', { ready: platformReadySpy });

    TestBed.configureTestingModule({
        declarations: [AppComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        imports: [
            HttpClientTestingModule,
            TranslateModule.forRoot({
                loader: {
                    provide: TranslateLoader,
                    deps: [HttpClient]
                }
            })
        ],
        providers: [
            {provide: StatusBar, useValue: statusBarSpy},
            {provide: SplashScreen, useValue: splashScreenSpy},
            {provide: Platform, useValue: platformSpy},
            {provide: TranslateService, useValue: translate}
        ],
    }).compileComponents();

    translate = TestBed.get(TranslateService);
    http = TestBed.get(HttpTestingController);

  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should initialize the app', async () => {

    //  spyOn(translate, 'getBrowserLang').and.returnValue('de');

    TestBed.createComponent(AppComponent);
    expect(platformSpy.ready).toHaveBeenCalled();
    await platformReadySpy;
    expect(statusBarSpy.styleDefault).toHaveBeenCalled();
    expect(splashScreenSpy.hide).toHaveBeenCalled();
  });


  it('should load translations', async () => {

      spyOn(translate, 'getBrowserLang').and.returnValue('de');

      const fixture = TestBed.createComponent(AppComponent);
      const compiled = fixture.debugElement.nativeElement;

      // the DOM should be empty for now since the translations haven't been rendered yet
      expect(compiled.querySelector('h2').textContent).toEqual('');

      http.expectOne('/assets/i18n/de.json').flush(TRANSLATIONS_DE);
      http.expectNone('/assets/i18n/en.json');

      // Finally, assert that there are no outstanding requests.
      http.verify();

      fixture.detectChanges();
      // the content should be translated to english now
      expect(compiled.querySelector('h2').textContent).toEqual(TRANSLATIONS_DE.HOME.TITLE);

      translate.use('en');
      http.expectOne('/assets/i18n/en.json').flush(TRANSLATIONS_EN);

      // Finally, assert that there are no outstanding requests.
      http.verify();

      // the content has not changed yet
      expect(compiled.querySelector('h2').textContent).toEqual(TRANSLATIONS_DE.HOME.TITLE);

      fixture.detectChanges();
      // the content should be translated to french now
      expect(compiled.querySelector('h2').textContent).toEqual(TRANSLATIONS_EN.HOME.TITLE);

  });


/*
  it('should load translations', async(() => {
      spyOn(translate, 'getBrowserLang').and.returnValue('de');
      const fixture = TestBed.createComponent(AppComponent);
      const compiled = fixture.debugElement.nativeElement;

      // the DOM should be empty for now since the translations haven't been rendered yet
      expect(compiled.querySelector('h2').textContent).toEqual('');

      http.expectOne('/assets/i18n/de.json').flush(TRANSLATIONS_DE);
      http.expectNone('/assets/i18n/en.json');

      // Finally, assert that there are no outstanding requests.
      http.verify();

      fixture.detectChanges();
      // the content should be translated to english now
      expect(compiled.querySelector('h2').textContent).toEqual(TRANSLATIONS_DE.HOME.TITLE);

      translate.use('en');
      http.expectOne('/assets/i18n/en.json').flush(TRANSLATIONS_EN);

      // Finally, assert that there are no outstanding requests.
      http.verify();

      // the content has not changed yet
      expect(compiled.querySelector('h2').textContent).toEqual(TRANSLATIONS_DE.HOME.TITLE);

      fixture.detectChanges();
      // the content should be translated to french now
      expect(compiled.querySelector('h2').textContent).toEqual(TRANSLATIONS_EN.HOME.TITLE);
  }));
  */
  // TODO: add more tests!

});
