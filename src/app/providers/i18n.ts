import { APP_CONFIG } from "../../app/app.config";
import { GfStorageProvider } from "../providers/gf-storage";
import { Platform } from '@ionic/angular';
import { TranslateService } from "@ngx-translate/core";

//@Injectable()
export class  I18nProvider {

    language: string;
    constructor(
        private translate: TranslateService,
        private gfStorage: GfStorageProvider,
        private platform: Platform
    ){}


    async setDefaultLanguage() {
        console.log("- - - - -setDefaultLanguage - - -");
        // Get user saved language from storage
        const userLang = await this.gfStorage.getUserLanguage();
        console.log('user language is', userLang);

        // If not found get browser preferred language
        if(!userLang) {
            console.log('!!!! preferred language', this.translate.getBrowserLang());
            const preferredLang = this.translate.getBrowserLang();

            if(preferredLang!="de")
                this.language = "en";
            else{
                if (preferredLang) {
                    this.language = preferredLang;
                } else {
                    // If not found default to app default language
                    this.language = APP_CONFIG.DEFAULT_LANGUAGE;
                }
            }
        } else {
            this.language = userLang;
        }
        // this.language = "ar";
        this.translate.use(this.language);

        // Save current language to state
      //  this.state.language = this.language;
      //  this.setLangDirection(this.language);
    }

    async changeLanguage(lang: string) {
        // Change user language in storage
        try {
            const setLang = await this.gfStorage.setUserLanguage(lang);
            console.log('set lang', setLang);
            this.translate.use(setLang);
           // this.state.language = setLang;
           // this.setLangDirection(setLang);
            //this.events.publish('language:changed', lang);
            this.language = lang;
        } catch (error) {
            console.log(error);
        }
    }
}