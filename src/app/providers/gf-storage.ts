import { Injectable } from '@angular/core';
//import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
@Injectable()
export class GfStorageProvider {

    constructor(
        private platform: Platform,
        private storage: Storage
    ) {
    }

    async storageReady() {
        await this.platform.ready();
        await this.storage.create();
    }
    async setUserLanguage(lang: string) {
        await this.platform.ready();
        return await this.storage.set('language', lang);
    }

    async getUserLanguage() {
        await this.platform.ready();
        return await this.storage.get('language');
    }

}
