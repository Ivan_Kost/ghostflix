import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from "../services/auth.service";
import { APP_CONFIG } from '../app.config';

@Component({
  selector: 'app-restricted',
  templateUrl: './restricted.page.html',
  styleUrls: ['./restricted.page.scss'],
})
export class RestrictedPage implements OnInit {

  loggedIn: boolean = false;
  logoImage: any;

  constructor(private router: Router,
              private auth: AuthService) { }

  ngOnInit() {
    this.logoImage = APP_CONFIG.LOGO_IMAGE;
    this.isLoggedIn();
  }

  goToLogin() {
    this.router.navigate(['/login']);
  }

  goToHome(){
    this.router.navigateByUrl('/tabs/staffeln');
  }

  goToRegister() {
    this.router.navigate(['/register']);
  }

  isLoggedIn() {
    this.auth.isUserLoggedIn().then((response) => {
      console.log(response)
      if (response['error']) {
        console.log('user not logged in, clear user session data');
        localStorage.setItem('currentUserId', null);
        localStorage.setItem('access_token', null);
        localStorage.setItem('display_name', null);
      } else {
        if(response['code'] ==="jwt_auth_valid_token"){
          this.loggedIn = true;
        }
      }
    });
  }

    impressum() {
        this.router.navigate(['/impressum']);
    }

    datenschutzerklarung() {
        this.router.navigate(['/datenschutzerklarung']);
    }

}
