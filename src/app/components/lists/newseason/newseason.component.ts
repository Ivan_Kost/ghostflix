import { Component, OnInit } from '@angular/core';
import {WordpressService} from '../../../services/wordpress.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-newseason',
  templateUrl: './newseason.component.html',
  styleUrls: ['./newseason.component.scss'],
})
export class NewseasonComponent implements OnInit {
    index;
    type: string;
    videos: any;

    constructor(
        private WP: WordpressService,
        private router: Router
    ) {
    }

    ngOnInit() {
        const splurl = this.router.url.split('/');
        this.type = splurl[splurl.length - 1];

        this.getSeason();
    }

    ionViewWillEnter() {
        if (!this.videos)
            this.getSeason();
    }

    goToSeason(slug) {
        this.router.navigate(['tabs/video-detail', slug]).then();
    }

    getSeason() {
        this.WP.getVideos(this.type).subscribe((data) => {
            this.videos = data;
            console.log('Season videos : ', data);
        });
    }

}