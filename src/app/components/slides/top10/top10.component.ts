import { Component, OnInit, ViewChild } from '@angular/core';
import { WordpressService } from '../../../services/wordpress.service';
import { Router } from '@angular/router';
import {ActionSheetController, IonSlides, ModalController, Platform} from '@ionic/angular';
import { InfoVideoModalComponent } from '../../modals/info-video-modal/info-video-modal.component';
import { APP_CONFIG } from '../../../app.config';
import {CommonfunctionService} from '../../../services/commonfunction.service';

@Component({
  selector: 'app-top10-component',
  templateUrl: './top10.component.html',
  styleUrls: ['./top10.component.scss'],
})
export class Top10Component implements OnInit {

    @ViewChild('slideWithNavTop10', {static: false}) slideWithNavTop10: IonSlides;

    index;
    slides: any;
    sliderTop10: any;
    defaultImg: any;
    platformType: string;

    slideOptsTop10 = {
        initialSlide: 0,  // Slide Index Starting from 0
        slidesPerView: 2, // Slides Visible in Single View Default is 1
        loop: true,
        //centeredSlides: true,
        spaceBetween: 5,
        speed: 400
    };


    constructor(
        private WP: WordpressService,
        private CFS: CommonfunctionService,
        private modalController: ModalController,
        private actionSheetController: ActionSheetController,
        private router: Router,
        public platform: Platform,
    ) {
        //Configuration for Top Slider
        this.sliderTop10 =
            {
                isBeginningSlide: true,
                isEndSlide: false,
                slidesItems: []
            };
        this.defaultImg = APP_CONFIG.DEFAULT_IMAGE;
        this.platformType = this.platform.is('tablet') ? 'tablet' : 'mobile';

    }

    ngOnInit() {
        this.getTop10Sliders();
    }

    ionViewWillUnload() {
        if (!this.slides)
            this.getTop10Sliders();
    }

    goToSlide(slug) {
        this.router.navigate(['tabs/video-detail', slug]).then();
    }

    getTop10Sliders() {
        this.WP.getTypeSlide('top-ten').subscribe((data) => {
            this.slides = data;
            this.sliderTop10.slidesItems = data;
            console.log('Top10 Slides: ', this.slides);
            //  this.slider.startAutoplay;
        });
    }

    //Move to Next slide
    slideNext(object, slideView) {
        slideView.slideNext(500).then(() => {
            this.checkIfNavDisabled(object, slideView);
        });
    }

    //Move to previous slide
    slidePrev(object, slideView) {
        slideView.slidePrev(500).then(() => {
            this.checkIfNavDisabled(object, slideView);
        });
    }

    //Method called when slide is changed by drag or navigation
    SlideDidChange(object, slideView) {
        this.checkIfNavDisabled(object, slideView);
    }

    //Call methods to check if slide is first or last to enable disbale navigation
    checkIfNavDisabled(object, slideView) {
        this.checkisBeginning(object, slideView);
        this.checkisEnd(object, slideView);
    }

    checkisBeginning(object, slideView) {
        slideView.isBeginning().then((istrue) => {
            object.isBeginningSlide = istrue;
        });
    }

    checkisEnd(object, slideView) {
        slideView.isEnd().then((istrue) => {
            object.isEndSlide = istrue;
        });
    }

    async openInfoModal(item) {
        console.log('openInfoModal! ', item);
        const modal = await this.modalController.create({
            component: InfoVideoModalComponent,
            cssClass: 'small-modal',
            swipeToClose: true,
            showBackdrop: true,
            componentProps: {
                vimeoId: item.id,
                description: item.excerpt,
                image: item.thumbnail,
                release_date: item.release_date,
                vimeo_video: item.vimeo_video,
                season: item.slug,
                skipIntroTime: item.skip_intro_time,
                episode_title: item.title,
                slug: item.slug
            },
        });

        modal.onDidDismiss().then((modalDataResponse) => {
            if (modalDataResponse !== null) {
                //   this.modalDataResponse = modalDataResponse.data;
                console.log('Modal Sent Data : ', modalDataResponse.data);
            }
        });
        return await modal.present();
    }

    async openMore(slide) {
        console.log('MORE slide:', slide);

        const actionSheet = await this.actionSheetController.create({
            header: slide.episode_title,
            cssClass: 'info-action-sheet',
            buttons: [{
                text: 'Folgen und infos',
                role: 'destructive',
                icon: 'information-circle',
                cssClass: 'informationIcon',
                handler: () => {
                    console.log('Information');
                    this.videoDetail(slide.slug);
                }
            }, {
                text: (!slide.is_favorite ? 'Zu Favoriten hinzufügen' : 'Favorite'),
                icon: (!slide.is_favorite ? 'thumbs-up-outline' : 'heart'),
                cssClass: 'goodIcon',
                handler: () => {
                    console.log('Favorite');
                    const result = (!slide.is_favorite) ? this.addToFavorites(slide) : this.removeFromFavorites(slide);
                   // const resultText = (!slide.is_favorite) ? 'Has been added to the Favorites' : 'Has been deleted off the Favorites';
                    const resultText = (!slide.is_favorite) ? 'Wurde zu den Favoriten hinzugefügt' : 'Wurde aus den Favoriten gelöscht';
                    this.CFS.presentToast(resultText, 3000);
                /*    if (result)
                        slide.is_favorite = (!slide.is_favorite) ? true : false;
            */    }
            }, {
                text: (!slide.in_watchlist ? 'Zur Watchlist hinzufügen' : 'Watchlist'),
                icon: (!slide.in_watchlist ? 'add' : 'checkmark-outline'),
                cssClass: 'badlyIcon',
                handler: () => {
                    console.log('Watchlist');
                    const result = (!slide.in_watchlist) ? this.addToWatchList(slide) : this.removeFromWatchList(slide);
                    const resultText = (!slide.in_watchlist) ? 'Wurde zu den Watchlist hinzugefügt' : 'Wurde aus den Watchlist gelöscht';
                    this.CFS.presentToast(resultText, 3000);
              /*      console.log('result:', result);
                    if (result)
                        slide.in_watchlist = (!slide.in_watchlist) ? true : false;
          */      }
            }, {
                text: 'Schließen',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    console.log('Remove from row');
                }
            }]
        });
        await actionSheet.present();
    }

    async addToFavorites(video) {
        this.WP.addToFavorites(video.id).subscribe((data) => {
            console.log(data.success);
            if(data.success === true){
                video.is_favorite = true;
            }
        });
    }

    async removeFromFavorites(video) {
        this.WP.removeFromFavorites(video.id).subscribe((data) => {
            if(data.success === true){
                video.is_favorite = false;
            }
        });
    }

    async addToWatchList(video) {
        const result = this.WP.addToWatchList(video.id).subscribe((data) => {
            if(data.success === true){
                video.in_watchlist = true;
            }
        });
    }

    async removeFromWatchList(video) {
        const result = this.WP.removeFromWatchList(video.id).subscribe((data) => {
            if(data.success === true){
                video.in_watchlist = false;
            }
        });
    }

    videoDetail(slug) {
        this.router.navigate(['tabs/video-detail', slug]).then();
    }
}