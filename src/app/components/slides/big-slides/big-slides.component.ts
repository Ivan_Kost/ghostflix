import { Component, OnInit, ViewChild } from '@angular/core';
import { WordpressService } from '../../../services/wordpress.service';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-big-slides',
  templateUrl: './big-slides.component.html',
  styleUrls: ['./big-slides.component.scss'],
})
export class BigSlidesComponent implements OnInit {

    @ViewChild('slideWithNavSlides', {static: false}) slideWithNavSlides: IonSlides;

    index;
    slides: any;
    sliderSlides: any;

    slideOptsSlides = {
        initialSlide: 0,  // Slide Index Starting from 0
        slidesPerView: 1, // Slides Visible in Single View Default is 1
        loop: true,
        centeredSlides: true,
        spaceBetween: 0,
        speed: 400,
        autoplay: {
            delay: 5000,
        },
    };


    constructor(
        private WP: WordpressService,
        private router: Router
    ) {
        //Configuration for Slider
        this.sliderSlides =
            {
                isBeginningSlide: true,
                isEndSlide: false,
                slidesItems: []
            };
    }

    ngOnInit() {
        this.getSlidesSliders();
    }

    ionViewWillEnter() {
        if (!this.slides)
            this.ngOnInit();
    }

    goToSlide(slug) {
        // this.router.navigate(['tabs/video-detail', slug]).then();
    }

    getSlidesSliders() {
        this.WP.getTypeSlide('slides').subscribe((data) => {
            this.slides = data;
            this.sliderSlides.slidesItems = data;
            console.log('Slides: ', this.slides);
            //  this.slider.startAutoplay;
        });
    }

    //Move to Next slide
    slideNext(object, slideView) {
        slideView.slideNext(500).then(() => {
            this.checkIfNavDisabled(object, slideView);
        });
    }

    //Move to previous slide
    slidePrev(object, slideView) {
        slideView.slidePrev(500).then(() => {
            this.checkIfNavDisabled(object, slideView);
        });
    }

    //Method called when slide is changed by drag or navigation
    SlideDidChange(object, slideView) {
        this.checkIfNavDisabled(object, slideView);
    }

    //Call methods to check if slide is first or last to enable disbale navigation
    checkIfNavDisabled(object, slideView) {
        this.checkisBeginning(object, slideView);
        this.checkisEnd(object, slideView);
    }

    checkisBeginning(object, slideView) {
        slideView.isBeginning().then((istrue) => {
            object.isBeginningSlide = istrue;
        });
    }

    checkisEnd(object, slideView) {
        slideView.isEnd().then((istrue) => {
            object.isEndSlide = istrue;
        });
    }

}