import { Component, OnInit, ViewChild } from '@angular/core';
import { WordpressService } from '../../../services/wordpress.service';
import { Router } from '@angular/router';
import { ActionSheetController, IonSlides, ModalController} from '@ionic/angular';
import { APP_CONFIG } from '../../../app.config';
import { TranslateService } from '@ngx-translate/core';
import { InfoVideoModalComponent } from '../../modals/info-video-modal/info-video-modal.component';
import {CommonfunctionService} from '../../../services/commonfunction.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-season',
  templateUrl: './season.component.html',
  styleUrls: ['./season.component.scss'],
})
export class SeasonComponent implements OnInit {
    @ViewChild('slideWithNavSeason', {static: false}) slideWithNavSeason: IonSlides;
    index;
    type: string;
    slides: any;
    videos: any
    sliderSeason: any;
    newVideo: any;
    platformType: string;
    userName;
    userNameInfo;

    slideOptsSeason = {
        initialSlide: 0,  // Slide Index Starting from 0
        slidesPerView: 2, // Slides Visible in Single View Default is 1
        loop: false,
        //centeredSlides: true,
        spaceBetween: 6,
        speed: 400,
        preloadImages: false,

    };

    constructor(
        private WP: WordpressService,
        public CFS: CommonfunctionService,
        public translate: TranslateService,
        private modalController: ModalController,
        private actionSheetController: ActionSheetController,
        private router: Router,
        public platform: Platform,
    ) {
        //Configuration for Top Slider
        this.sliderSeason =
            {
                isBeginningSlide: true,
                isEndSlide: false,
                slidesItems: []
            };
        this.platformType = this.platform.is('tablet') ? 'tablet' : 'mobile';

    }

    ngOnInit() {
        this.newVideo = APP_CONFIG.NEW_VIDEO;
        const splurl = this.router.url.split('/');
        this.type = splurl[splurl.length - 1];

        this.getUserName();
        this.getSeasonSliders();

    }

    ionViewWillEnter() {
        if (!this.videos)
            this.getSeasonSliders();
    }

    async getUserName() {
        const trans = await this.translate.get([
            'HOME.CONTINUE_WATCHING'
        ]).toPromise();

        this.userName = localStorage.getItem('display_name');

        if (this.userName === 'null' || this.userName === null || this.userName === 'undefined' || this.userName === '') {
            this.userNameInfo = '';
        } else {
            if (trans)
                this.userNameInfo = trans['HOME.CONTINUE_WATCHING'];
            /*'Mit dem Profile von ' + this.userName + 'Weiterschauen';*/
        }
    }

    goToSlide(slug) {
        this.router.navigate(['tabs/video-detail', slug]).then();
    }

    getSeasonSliders() {
        this.WP.getVideos(this.type).subscribe((data) => {
            this.videos = data;
            //console.log('this.type : ', this.type);
            //console.log('Season videos : ', data);
        });
    }

    //Move to Next slide
    slideNext(object, slideView) {
        slideView.slideNext(500).then(() => {
            this.checkIfNavDisabled(object, slideView);
        });
    }

    //Move to previous slide
    slidePrev(object, slideView) {
        slideView.slidePrev(500).then(() => {
            this.checkIfNavDisabled(object, slideView);
        });
    }

    //Method called when slide is changed by drag or navigation
    SlideDidChange(object, slideView) {
        this.checkIfNavDisabled(object, slideView);
    }

    //Call methods to check if slide is first or last to enable disbale navigation
    checkIfNavDisabled(object, slideView) {
        this.checkisBeginning(object, slideView);
        this.checkisEnd(object, slideView);
    }

    checkisBeginning(object, slideView) {
        slideView.isBeginning().then((istrue) => {
            object.isBeginningSlide = istrue;
        });
    }

    checkisEnd(object, slideView) {
        slideView.isEnd().then((istrue) => {
            object.isEndSlide = istrue;
        });
    }

    async openInfoModal(item) {
        console.log('openInfoModal! ', item);
        const modal = await this.modalController.create({
            component: InfoVideoModalComponent,
            cssClass: 'small-modal',
            swipeToClose: true,
            showBackdrop: true,
            componentProps: {
                vimeoId: item.id,
                description: item.excerpt,
                image: item.thumbnail,
                release_date: item.release_date,
                vimeo_video: item.vimeo_video,
                season: item.slug,
                skipIntroTime: item.skip_intro_time,
                episode_title: item.title,
                slug: item.slug
            },

        });

        modal.onDidDismiss().then((modalDataResponse) => {
            if (modalDataResponse !== null) {
                //   this.modalDataResponse = modalDataResponse.data;
                console.log('Modal Sent Data : ', modalDataResponse.data);
            }
            //window.location.reload();
        });

        return await modal.present();
    }

    async openMore(slide) {
        console.log('MORE slide:', slide);

        const actionSheet = await this.actionSheetController.create({
            header: slide.episode_title,
            cssClass: 'info-action-sheet',
            buttons: [{
                text: 'Folgen und infos',
                role: 'destructive',
                icon: 'information-circle',
                cssClass: 'informationIcon',
                handler: () => {
                    console.log('Information');
                    this.videoDetail(slide.slug);
                }
            }, {
                text: (!slide.is_favorite ? 'Zu Favoriten hinzufügen' : 'Favorite'),
                icon: (!slide.is_favorite ? 'thumbs-up-outline' : 'heart'),
                cssClass: 'goodIcon',
                handler: () => {
                    const result = (!slide.is_favorite) ? this.addToFavorites(slide) : this.removeFromFavorites(slide);
                    const resultText = (!slide.is_favorite) ? 'Wurde zu den Favoriten hinzugefügt' : 'Wurde aus den Favoriten gelöscht';
                    this.CFS.presentToast(resultText, 3000);
                    /* if (result)
                         slide.is_favorite = (!slide.is_favorite) ? true : false;*/
                }
            }, {
                text: (!slide.in_watchlist ? 'Zur Watchlist hinzufügen' : 'Watchlist'),
                icon: (!slide.in_watchlist ? 'add' : 'checkmark-outline'),
                cssClass: 'badlyIcon',
                handler: () => {
                    const result = (!slide.in_watchlist) ? this.addToWatchList(slide) : this.removeFromWatchList(slide);
                    const resultText = (!slide.in_watchlist) ? 'Wurde zu den Watchlist hinzugefügt' : 'Wurde aus den Watchlist gelöscht';
                    this.CFS.presentToast(resultText, 3000);
                    /* console.log('result:', result);
                     if (result)
                         slide.in_watchlist = (!slide.in_watchlist) ? true : false;*/
                }
            }, {
                text: 'Schließen',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    console.log('Remove from row');
                }
            }]
        });
        await actionSheet.present();
    }

    async addToFavorites(video) {
        this.WP.addToFavorites(video.id).subscribe((data) => {
            if(data.success === true){
                video.is_favorite = true;
            }
        });
    }

    async removeFromFavorites(video) {
        this.WP.removeFromFavorites(video.id).subscribe((data) => {
            if(data.success === true){
                video.is_favorite = false;
            }
        });
    }

    async addToWatchList(video) {
        const result = this.WP.addToWatchList(video.id).subscribe((data) => {
            if(data.success === true){
                video.in_watchlist = true;
            }
        });
    }

    async removeFromWatchList(video) {
        const result = this.WP.removeFromWatchList(video.id).subscribe((data) => {
            if(data.success === true){
                video.in_watchlist = false;
            }
        });
    }

    videoDetail(slug) {
        this.router.navigate(['tabs/video-detail', slug]).then();
    }

}