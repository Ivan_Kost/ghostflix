import {Component, OnInit, ViewChild} from '@angular/core';
import {IonSlides, ModalController, ActionSheetController, Platform} from '@ionic/angular';
import {WordpressService} from '../../../services/wordpress.service';
//import {PlayerPage} from '../../../pages/player/player.page';
import {InfoVideoModalComponent} from '../../modals/info-video-modal/info-video-modal.component';
import {Router} from '@angular/router';
import { APP_CONFIG } from '../../../app.config';
import { TranslateService } from '@ngx-translate/core';
import { VideoJSplayerPage } from '../../../pages/video-jsplayer/video-jsplayer.page';
import {PlayerPage} from '../../../pages/player/player.page';
import {CommonfunctionService} from '../../../services/commonfunction.service';



@Component({
    selector: 'app-continue-watching',
    templateUrl: './continue-watching.component.html',
    styleUrls: ['./continue-watching.component.scss'],
})
export class ContinueWatchingComponent implements OnInit {

    @ViewChild('slideWithNavontinueContinueWatching', { static: false }) slideWithNavontinueContinueWatching: IonSlides;

    index;
    type: string;
    slides: any;
    sliderContinueWatching: any;
    newVideo: any;
    platformType: string;
    userData: any;

    slideOptsContinueWatching = {
        initialSlide: 0,  // Slide Index Starting from 0
        slidesPerView: 2, // Slides Visible in Single View Default is 1
        loop: true,
        //centeredSlides: true,
        spaceBetween: 8,
        speed: 400
    };

    constructor(
        private modalController: ModalController,
        private actionSheetController: ActionSheetController,
        private router: Router,
        public translate: TranslateService,
        public CFS: CommonfunctionService,
        private WP: WordpressService,
        public platform: Platform,
    ) {
        //Configuration for ContinueWatching Slider
        this.sliderContinueWatching =
            {
                isBeginningSlide: true,
                isEndSlide: false,
                slidesItems: [],
                preloadImages: false,
            };
        this.platformType = this.platform.is('tablet') ? 'tablet' : 'mobile';

    }

    ngOnInit() {
       // this.translate.setDefaultLang('de');
        this.newVideo = APP_CONFIG.NEW_VIDEO;
        this.getContinueWatchingSliders();
        this.getUserData();
    }

    ngAfterViewInit() {
        console.log("- - - - ngAfterViewInit - - - ");
       // if (!this.slides)
            this.getContinueWatchingSliders();
    }
    goToSlide(slideID){
        console.log('Go to slide ', slideID);
    }

    async onPlay( item ) {
        if(!(this.userData && this.userData.has_access)){
            this.router.navigate(['/restricted']).then();
            return;
        }

        screen.orientation.unlock();
        screen.orientation.lock('landscape').then(() => {
            console.log('orientation is  supported');

        }).catch(err => console.log('Orientation error: ', err));

        if (item.mp4_video) {
            console.log('* * * item: ', item);
            this.WP.getSignUrl(item.mp4_video, item.mp4_video_hd, item.textrack_vtt).subscribe((data) => {
                console.log('* * * getSignUrl: ', data);
                //this.goToModal(item, data.signed_url.video, data.signed_url.texttrack, VideoJSplayerPage);
                this.goToModal(item, data, data.signed_url.texttrack, VideoJSplayerPage);
            });
        } else {
            this.goToModal(item, '', '', PlayerPage);
        }
    }

    async goToModal(item, data, subtitleURL, playerType) {
        var mp4Video = (data != '' ? (data.signed_url.video ? data.signed_url.video : item.mp4_video) : item.mp4_video);
        var mp4Video_hd = (data != '' ? data.signed_url.video_hd : item.mp4_video_hd);
        var awsUrl_ = (data != '' ? data.signed_url.video : '');
        console.log('* * * mp4Video: ', mp4Video);
        console.log('* * * mp4Video_hd: ', mp4Video_hd);
        console.log('* * * awsUrl_: ', awsUrl_);
        console.log('* * * item: ', item);

        const modal = await this.modalController.create({
            component: playerType, // VideoJSplayerPage, //PlayerPage,
            cssClass: 'fullScreen',
            componentProps: {
                vimeoId: item.vimeo_video,
                mp4_video: mp4Video,
                mp4_video_hd: mp4Video_hd,
                poster_image: item.poster_image,
                subtitlesOn: item.force_subtitles,
                videoId: item.video_id,
                time: item.time,
                progress: item.progress,
                season: item.slug,
                skipIntroTime: item.skip_intro_time,
                subtitle: subtitleURL,
                aws_url: awsUrl_ //data.signed_url.video //awsUrl
            }
        });
        modal.onDidDismiss().then((data) => {
            screen.orientation.unlock();
            console.log('data modal video', data);
            item.time = data.data.time;
            item.progress = data.data.progress;
            item.is_watched = data.data.is_watched;
            //window.location.assign('/');
            this.getContinueWatchingSliders();
        });
        return await modal.present();

    }

    async openInfoModal(item){
        console.log("openInfoModal! ", item);
        const modal = await this.modalController.create({
            component: InfoVideoModalComponent,
            cssClass: 'small-modal',
            swipeToClose: true,
            showBackdrop: true,
            componentProps: {
                vimeoId: item.season_id,
                description: item.description,
                image: item.episode_image,
                release_date: item.release_date,
                vimeo_video: item.vimeo_video,
                season: item.slug,
                skipIntroTime: item.skip_intro_time,
                episode_title: item.episode_title,
                slug: item.slug
            },

        });

        modal.onDidDismiss().then((modalDataResponse) => {
            if (modalDataResponse !== null) {
             //   this.modalDataResponse = modalDataResponse.data;
                console.log('Modal Sent Data : ', modalDataResponse.data);
            }
            window.location.reload();

        });
        //ionModalDidDismiss

        return await modal.present();
    }
    
    async openMore(slide) {
        console.log('MORE slide:', slide);

        const actionSheet = await this.actionSheetController.create({
            header: slide.episode_title,
            cssClass: 'info-action-sheet',
            buttons: [{
                text: 'Folgen und infos',
                role: 'destructive',
                icon: 'information-circle',
                cssClass: 'informationIcon',
                handler: () => {
                    console.log('Information');
                    this.videoDetail(slide.slug);
                }
            }, {
                text: (!slide.is_favorite ? 'Zu Favoriten hinzufügen' : 'Favorite'),
                icon: (!slide.is_favorite ? 'thumbs-up-outline' : 'heart'),
                cssClass: 'goodIcon',
                handler: () => {
                    console.log('Favorite');
                    const resultText = (!slide.is_favorite) ? 'Wurde zu den Favoriten hinzugefügt' : 'Wurde aus den Favoriten gelöscht';
                    this.CFS.presentToast(resultText, 3000);
                    const result = (!slide.is_favorite) ? this.addToFavorites(slide) : this.removeFromFavorites(slide);
                    if (result)
                        slide.is_favorite = (!slide.is_favorite) ? true : false;
                }
            }, {
                text: (!slide.in_watchlist ? 'Zur Watchlist hinzufügen' : 'Watchlist'),
                icon: (!slide.in_watchlist ? 'add' : 'checkmark-outline'),
                cssClass: 'badlyIcon',
                handler: () => {
                    console.log('Watchlist');
                    const result = (!slide.in_watchlist) ? this.addToWatchList(slide) : this.removeFromWatchList(slide);
                    const resultText = (!slide.in_watchlist) ? 'Wurde zu den Watchlist hinzugefügt' : 'Wurde aus den Watchlist gelöscht';
                    this.CFS.presentToast(resultText, 3000);
                    console.log('result:', result);
                    if (result)
                        slide.in_watchlist = (!slide.in_watchlist) ? true : false;
                }
            }, {
                text: 'Schließen',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    console.log('Remove from row');
                }
            }]
        });
        await actionSheet.present();
    }

    async addToFavorites(video){
        this.WP.addToFavorites(video.season_id).subscribe((data) => {
            console.log(data.success);
        });
    }
    async removeFromFavorites(video){
        this.WP.removeFromFavorites(video.season_id).subscribe((data) => {
            console.log(data.success);
        });
    }
    async addToWatchList(video){
        const result = this.WP.addToWatchList(video.season_id).subscribe((data) => {
            console.log(data.success);
        });
    }
    async removeFromWatchList(video){
        const result = this.WP.removeFromWatchList(video.season_id).subscribe((data) => {
            console.log(data.success);
        });
    }

    videoDetail(slug) {
        this.router.navigate(['tabs/video-detail', slug]).then();
    }

    getContinueWatchingSliders() {
        this.WP.getTypeSlide('continue-watching').subscribe((data) => {
            this.slides = data;
            this.sliderContinueWatching.slidesItems = data;
            console.log('ContinueWatching Slides: ', this.slides);
            //  this.slider.startAutoplay;
        });
    }

    getUserData() {
        this.WP.getUserStatus().subscribe((data) => {
            this.userData = data;
        })
    }

    //Move to Next slide
    slideNext(object, slideView) {
        slideView.slideNext(500).then(() => {
            this.checkIfNavDisabled(object, slideView);
        });
    }

    //Move to previous slide
    slidePrev(object, slideView) {
        slideView.slidePrev(500).then(() => {
            this.checkIfNavDisabled(object, slideView);
        });
    }

    //Method called when slide is changed by drag or navigation
    SlideDidChange(object, slideView) {
        this.checkIfNavDisabled(object, slideView);
    }

    //Call methods to check if slide is first or last to enable disbale navigation
    checkIfNavDisabled(object, slideView) {
        this.checkisBeginning(object, slideView);
        this.checkisEnd(object, slideView);
    }

    checkisBeginning(object, slideView) {
        slideView.isBeginning().then((istrue) => {
            object.isBeginningSlide = istrue;
        });
    }
    checkisEnd(object, slideView) {
        slideView.isEnd().then((istrue) => {
            object.isEndSlide = istrue;
        });
    }

}
