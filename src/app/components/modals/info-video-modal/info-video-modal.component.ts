import {Component, Input, OnInit} from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-info-video-modal',
  templateUrl: './info-video-modal.component.html',
  styleUrls: ['./info-video-modal.component.scss'],
})
export class InfoVideoModalComponent implements OnInit {
    @Input() vimeoId: string;
    @Input() description: string;
    @Input() image: string;
    @Input() release_date: string;
    @Input() vimeo_video: string;
    @Input() season: string;
    @Input() skipIntroTime: any;
    @Input() episode_title: any;
    @Input() slug: any;

    constructor(
        private modalCtr: ModalController,
        private router: Router
    ) {
    }

    ngOnInit() {
        console.log("vimeoId: ", this.vimeoId);
        console.log("description: ", this.description);
        console.log("image: ", this.image);
        console.log("release_date: ", this.release_date);
        console.log("vimeo_video: ", this.vimeo_video);
        console.log("season: ", this.season);
        console.log("skipIntroTime: ", this.skipIntroTime);
    }

    async closeBtn() {
        const closeModal: string = "Modal Closed";
        await this.modalCtr.dismiss(closeModal);
    }

    goToSlide(slug) {
        this.router.navigate(['tabs/video-detail', slug]).then();
        this.closeBtn();
    }

}
