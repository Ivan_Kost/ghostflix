import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Top10Component } from './slides/top10/top10.component';
import { ContinueWatchingComponent } from './slides/continue-watching/continue-watching.component';
import { InfoVideoModalComponent } from './modals/info-video-modal/info-video-modal.component';
import { BigSlidesComponent } from './slides/big-slides/big-slides.component';
import { SeasonComponent } from './slides/season/season.component';
import { NewseasonComponent } from './lists/newseason/newseason.component';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [
      Top10Component,
      ContinueWatchingComponent,
      BigSlidesComponent,
      SeasonComponent,
      NewseasonComponent,
      InfoVideoModalComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports:[
      Top10Component,
      ContinueWatchingComponent,
      BigSlidesComponent,
      SeasonComponent,
      NewseasonComponent,
      InfoVideoModalComponent
  ]
})
export class ComponentsModule { }
