export const APP_CONFIG = {
    DEFAULT_LANGUAGE: 'de',

    // PROD  //production_database
    DOMAIN: 'https://ghostflix.tv',
    // DEV
    // DOMAIN: 'https://b2ppkmq6.myraidbox.de',
    // TEST
    // DOMAIN: 'https://test.ghostflix.tv',
    JWT_PATH: '/wp-json/jwt-auth/v1/token',
    API_BASE: '/wp-json/api/v1/',
    API_WP_BASE: '/wp-json/wp/v2/',

    THEME: {
        PRIMARY: '#45a096',
        SECONDARY: '#3e3d3f'
    },
    LOGO_IMAGE: './assets/imgs/logo1.png',
    DEFAULT_IMAGE: './assets/imgs/default.png',
    NEW_VIDEO: 'NEUE FOLGEN'
};
