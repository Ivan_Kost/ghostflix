import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavigationBar } from '@ionic-native/navigation-bar/ngx';
//import { GfStorageProvider } from '../app/providers/gf-storage';
//import { I18nProvider } from "../app/providers/i18n";
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public translate: TranslateService,
    //private i18n: I18nProvider,
    //private gfStorage: GfStorageProvider,
    private navigationBar: NavigationBar
  ) {
      this.translate.addLangs(['de', 'en']);
      this.translate.setDefaultLang('de');
     /*
      const browserLang = translate.getBrowserLang();
      translate.use(browserLang.match(/de|en/) ? browserLang : 'de');
*/
      this.checkAuthStatus();
      this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.hide();
      this.statusBar.overlaysWebView(true);
      let autoHide: boolean = true;
      this.navigationBar.setUp(autoHide);
      this.splashScreen.hide();
    });
  }
  async checkAuthStatus() {
      console.log("Language :", this.translate.getDefaultLang());
     //  await this.i18n.setDefaultLanguage();
  }
  switchLang(lang: string) {
      this.translate.use(lang);
  }
}
