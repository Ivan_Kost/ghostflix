import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { APP_CONFIG } from '../app.config';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  currentSessionUser: any;
  isUserAuthenticated: boolean = false;

  constructor(private http: HttpClient) { }
  //#server_url
  siteURL: string = APP_CONFIG.DOMAIN;
  jwtPart: string = APP_CONFIG.JWT_PATH;
  apiURL: string = '';

  login(email, password) {
    let body = new URLSearchParams();
    body.set("username", email);
    body.set("password", password);
    body.set("log", email);
    body.set("pwd", password);
    let credentials = `username=${email}&password=${password}`;
    let headers = new HttpHeaders ({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    });
    this.apiURL = `${this.siteURL}${this.jwtPart}`;

    return new Promise((resolve) => {
      this.http.post(this.apiURL, body,{headers}).subscribe((successResp) => {
        resolve(successResp);
      },
      (errorResp) => {
        resolve(errorResp);
      }
      );
    });
  }

  isUserLoggedIn() {

    this.apiURL = `${this.siteURL}${this.jwtPart}/validate`;

    return new Promise((resolve) => {
      this.http.post(this.apiURL, {}, {}).subscribe((successResp) => {
            resolve(successResp);
          },
          (errorResp) => {
            resolve(errorResp);
          }
      );
    });

  }

  getAuthorizationToken() {
    const token = localStorage.getItem('access_token');
    console.log('Access token', token);
    return token;
  }

}
