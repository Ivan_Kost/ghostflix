import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {Router} from '@angular/router';
import { APP_CONFIG } from '../app.config';

@Injectable({
    providedIn: 'root'
})
export class WordpressService {

//#server_url
    siteUrl: string = APP_CONFIG.DOMAIN;
    apiBase = APP_CONFIG.API_BASE;
    apiWpBase = APP_CONFIG.API_WP_BASE;

    apiUrl = '';
    slides: any;
    videos: any;
    series: any;
    userData: any;
    successData: any;

    constructor(private http: HttpClient,
                private router: Router) {}

    getStartSlides() {
        this.apiUrl = `${this.siteUrl}${this.apiBase}slider/app_start_page`;
        this.slides = this.http.get(this.apiUrl);
        return this.slides;
    }


    getHomeSlides() {
        this.apiUrl = `${this.siteUrl}${this.apiBase}slider/app_home_page`;
        this.slides = this.http.get(this.apiUrl);
        return this.slides;
    }

    getSlides(slug) {
        this.apiUrl = `${this.siteUrl}${this.apiBase}slider/${slug}`;
        this.slides = this.http.get(this.apiUrl);
        return this.slides;
    }

    getTypeSlide(type) {
        this.apiUrl = `${this.siteUrl}${this.apiBase}${type}`;
        this.slides = this.http.get(this.apiUrl);
        return this.slides;
    }

    getSearchData(keyword) {
        this.apiUrl = `${this.siteUrl}${this.apiBase}search?q=${keyword}`;
        this.slides = this.http.get(this.apiUrl);
        return this.slides;
    }
    getHomeVideos() {
        this.apiUrl = `${this.siteUrl}${this.apiBase}videos/staffeln`;
        this.videos = this.http.get(this.apiUrl);
        return this.videos;
    }

    getVideos(type) {
        this.apiUrl = `${this.siteUrl}${this.apiBase}videos/${type}`;
        this.videos = this.http.get(this.apiUrl);
        this.videos.subscribe({
            next(x) { console.log('data: ', x); },
            error(err) { console.log('errors already caught... will not run', err);

            }
        });
        return this.videos;
    }

    getVideo(slug) {
        this.apiUrl = `${this.siteUrl}${this.apiBase}videos/${slug}/series`;
        this.series = this.http.get(this.apiUrl);
        return this.series;
    }

    getSignUrl(awsUrl?: string | null, videoHD?: string | null, subtitle?: string | null){
        this.apiUrl = `${this.siteUrl}${this.apiBase}sign-url`;
        this.series = this.http.post(this.apiUrl, {"aws_url":{ video: awsUrl, video_hd: videoHD, texttrack: subtitle }});
        return this.series;
    }

    getUserData(id) {
        this.apiUrl = `${this.siteUrl}${this.apiWpBase}users/${id}`;
        this.userData = this.http.get(this.apiUrl);
        return this.userData;
    }

    getUserVideoData() {
        this.apiUrl = `${this.siteUrl}${this.apiBase}user/data`;
        this.userData = this.http.get(this.apiUrl);
        return this.userData;
    }

    addToWatchList(id) {
        this.apiUrl = `${this.siteUrl}${this.apiBase}watch-list/add`;
        this.successData = this.http.post(this.apiUrl, { id});
        return this.successData;
    }

    addToFavorites(id) {
        this.apiUrl = `${this.siteUrl}${this.apiBase}favorites/add`;
        this.successData = this.http.post(this.apiUrl, { id});
        return this.successData;
    }

    removeFromWatchList(id) {
        this.apiUrl = `${this.siteUrl}${this.apiBase}watch-list/remove`;
        this.successData = this.http.post(this.apiUrl, { id});
        return this.successData;
    }

    removeFromFavorites(id) {
        this.apiUrl = `${this.siteUrl}${this.apiBase}favorites/remove`;
        this.successData = this.http.post(this.apiUrl, { id});
        return this.successData;
    }

    pushComment(rate, text, id, userId) {
        this.apiUrl = `${this.siteUrl}${this.apiBase}comments/add`;
        this.successData = this.http.post(this.apiUrl, {
            author: userId,
            content: text ,
            postId: id ,
            rating : rate });
        return this.successData;
    }

    saveTime(slug,video_id,time,duration) {
        console.log('WP service save time');
        this.apiUrl = `${this.siteUrl}${this.apiBase}watched-videos/${slug}/set-time`;
        this.successData = this.http.post(this.apiUrl, {
            time,
            duration,
            video_id
        });
        return this.successData;
    }

    setVideoAsWatched(slug,video_id){
        this.apiUrl = `${this.siteUrl}${this.apiBase}watched-videos/${slug}/add`;
        this.successData = this.http.post(this.apiUrl, {
            video_id,
            });
        return this.successData;
    }

    getUserStatus(){
        this.apiUrl = `${this.siteUrl}${this.apiBase}me`;
        return this.http.get(this.apiUrl);
    }
}
