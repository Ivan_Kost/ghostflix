import { Observable, Subject} from 'rxjs';
import { Injectable } from '@angular/core';
import { ToastController, AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CommonfunctionService {

  private userDisplayName = new Subject<any>();

  constructor(private toastController: ToastController, private alertController: AlertController) { }

  validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  async presentAlert(alrtHeader, alrtSubheader) {
    const alert = await this.alertController.create({
      header: alrtHeader,
      subHeader: alrtSubheader,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentToast(message, duration) {
    const toast = await this.toastController.create({
      message,
      duration
    });
    toast.present();
  }

  setDisplayName(displayname: string) {
    this.userDisplayName.next({display_name: displayname});
  }

  getDisplayName(): Observable<any> {
    return this.userDisplayName.asObservable();
  }

}
