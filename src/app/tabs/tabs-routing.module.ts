import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'staffeln',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/video-list/video-list.module').then(m => m.VideoListPageModule)
          }
        ]
      },
      {
        path: 'specials',
        children: [
          {
            path: '',
            loadChildren: () =>
                import('../pages/video-list/video-list.module').then(m => m.VideoListPageModule)
          }
        ]
      },
      {
        path: 'neu',
        children: [
          {
            path: '',
            loadChildren: () =>
                import('../pages/video-list/video-list.module').then(m => m.VideoListPageModule)
          }
        ]
      },
      {
        path: 'search',
        children: [
          {
            path: '',
            loadChildren: () =>
                import('../pages/search/search.module').then(m => m.SearchPageModule)
          }
        ]
      },
      {
        path: 'my-profile',
        children: [
          {
            path: '',
            loadChildren: () =>
                import('../pages/my-profile/my-profile.module').then(m => m.MyProfilePageModule)
          }
        ]
      },

      {
        path: 'video-detail/:video',
        children: [
          {
            path: '',
            loadChildren: () => import('../pages/video-detail/video-detail.module').then(m => m.VideoDetailPageModule)
          }
        ]
      },
      {
        path: 'search',
        children: [{
            path: '',
            loadChildren: () => import('../pages/search/search.module').then(m => m.SearchPageModule)
        }]
      },
      {
        path: 'login',
        redirectTo: '/login',
      },
      {
        path: '',
        redirectTo: '/tabs/staffeln',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/staffeln',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
