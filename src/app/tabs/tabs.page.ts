import { Component } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {NavigationBar} from '@ionic-native/navigation-bar/ngx';
import {Platform} from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  val;
  user_id;
  tabsName: any;

  constructor(private route: ActivatedRoute,
              private statusBar: StatusBar,
              private navigationBar: NavigationBar,
              public translate: TranslateService,
              private platform: Platform,
              private router: Router
              ) {
      this.initializeApp();
      this.tabsName = {
          'videos': '',
          'new': '',
          'search': '',
          'more': '',
          'login': ''
      };
  }

  tabChange(eve) {
    console.log(eve.tab);
    this.val = eve.tab;
    this.updateDynamicData();
  }

  initializeApp() {
    this.translateData();
    this.platform.ready().then(() => {
      this.statusBar.hide();
      this.statusBar.overlaysWebView(true);
      let autoHide: boolean = true;
      this.navigationBar.setUp(autoHide);
     this.updateDynamicData();
    });
  }

  updateDynamicData(){
    this.user_id = localStorage.getItem('currentUserId');
    if(this.val === 'login'){
      this.router.navigateByUrl('/login');
    }
    if(!this.val){
      this.val = 'staffeln';
    }
  }

  ionViewWillEnter() {
    this.user_id = localStorage.getItem('currentUserId');
  }

  async translateData() {
      const trans = await this.translate.get([
          'HOME.VIDEOS',
          'HOME.NEW',
          'SEARCH.TITLE',
          'HOME.MORE',
          'LOGIN.LOGIN'
      ]).toPromise();
      if (trans) {
          this.tabsName = {
              'videos': trans['HOME.VIDEOS'],
              'new': trans['HOME.NEW'],
              'search': trans['SEARCH.TITLE'],
              'more': trans['HOME.MORE'],
              'login': trans['LOGIN.LOGIN']
          };
      }
  }
}
